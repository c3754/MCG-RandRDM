# Reproducibility and RDM training

This repo contains the materials for the reproducibility and research data management training. Some basic instructions about git as provided by gitlab in their opening project Markdown can be found in this readme. Commits are made before each time the material is presented, so if you are looking for specific info from the session you attended, check the history of the project.

---

## This Repo

### Status:
The project is dormant.

### Contents:

1. Readme - This readme
2. Reproducibility_slides.rmd - Create the pdf slides used to present the material
3. Reproducibility-and-workflow-slides.pdf - The workshop slides (note that if the last commit on the pdf is not >= the commit time on the .rmd file that the pdf slides are not up-to-date)
4. License - CC BY NC
5. Exercises (API calls, git pull, and Haiku) which include code files in txt, python, R, and Stata.
6. Resources - Markdown & Git cheatsheets, Coding principles, *Energy, Economics, Replication & Reproduction*
7. Content (for slides) DHS_TOU and gitgui. (for slide theme) CRDCNtheme.tex and logo.png
8. Intro_and_content.rtf - Outline of the material and guide to coming prepared for the day.

These files can be compiled in any order and don't depend on each other. They can be run on any system which supports the software.

### Dependencies:

- The code in the exercises were written in R 4.2.2 *innocent and trusting*, Python3 (3.9.12), Stata 17. The examples should work in any reasonably modern version of R or Python3 (though I make no warranties). Stata 17 code relies on the dynamic document generation functionality included only in versions 17 or higher. 
- All the example code can be opened in any text editor
- The markdown code was compiled in Rstudio using knitr (which requires a LaTex installation). The markdown can be compiled into a plain version by deleting the appropriate YAML lines in the heading of the markdown slides.

### License:

This project is licensed as CC-BY-NC. The contents can be re-used by anyone for non-commercial purposes.  If you modify the materials before you present, you must credit me. While I do not require it, it would be a nice gesture to "share-alike" and licence your version under the same terms. A full description of the license is available in the repository.

---

# General Git guidelines (as provided by gitlab)

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps. There's links to a bunch of help-files linked below. Note that these are provided by git lab as part of their documentation.

### Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/c3754/MCG-RandRDM.git
git branch -M main
git push -uf origin main
```

### Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/c3754/MCG-RandRDM/-/settings/integrations)

### Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

### Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

---

# How to create a readme for a research repository?

### General guidelines for a good README
Every project is different, so consider what could be useful to yours. Keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing different forms of documentation rather than cutting out information.

### Name
Start with a title, title for the project, make it self-explanatory and short. It's best if the project title matches the repo-name because you'll have to use it from within the command-line interfaces for git.

### Description
Let people know what your project does specifically. Once you have published a paper based on the work, you should update your readme to reflect this. 

### Syntax (not a heading in your readme)
How do I create a markdown file? Well top level headings are single lines that start with #. Second-level headings start with ##. Third level headings start with ###. Note that you have to have a space between the octothorpe and the heading. Mostly though, you just type as you would normally. A few tips:

- [ ] as you see you can make a tick-box using a dash then square brackets
1. I can do numbered lists by just typing the number, the period and then my list
2. Like so
- There's also just ordinary lists which I create with a dash that turns into a bullet when it compiles.

There are other markdown syntax that can be used: 
- When typing, you can use asterix: one to make *italicized* text, and two to make **bolded** text 
- The horizontal lines you see in this file are created with three dashes in a row.
- See [This markdown guideline](https://www.markdownguide.org/cheat-sheet) for more syntax tips, like how to make tables, create sensible hyperlinks, highlights etc..

### Contents & Use
Provide a list of files in the repo as well as a brief description of what they do and how to use them to create your research output (e.g. first run the file "A", then "B" etc.).

### Dependencies & Computing environment
It's nice to give potential users the software needed to use the tools/code you're providing.

If you have a particular requirement for computing it's nice to mention here, otherwise, you can just add a note: "This code can be run on any system". One example that might help someone not ruin their day ex. "This code was run using a sharcnet allocation with 1TB of memory, and 64 compute cores. Sharcnet is a supercomputer provided by ComputeOntario and the Digital Research Alliance of Canada." Sometimes, your system is way over-spec for the code you're running. You might also have parts of your code that don't need the same resources. Try to be helpful!

### Support
*If you would like to* you can provide contact information for people to get help from you. You can also set your git profile to include your contact info if you'd like (you'll notice mine is private). If you are doing this, you might want to create an FAQ section directly above the support heading to head people off at the pass. You can also include a help file in your repo.

### Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

### License
For all projects, say how it is licensed (especially if you want to retain copyright). Note that this should just be a simple statement of the license. You should always add a full text of the license in a license file. I recommend not using the unlicense or cc0, but in some cases [it can be appropriate](https://theeggandtherock.substack.com/p/i-wrote-a-story-for-a-friend#%C2%A7creative-commons-licence-public-domain)
