require (dplyr, jsonlite)

readdata <- fromJSON("https://api.dhsprogram.com/rest/dhs/data?breakdown=national&indicatorIds=FE_FRTR_W_TFR&countryIds=EG,LB&surveyIds=EG2005DHS,EG2008DHS,EG2014DHS,LB2007DHS,LB2009MIS,LB2011MIS,LB2013DHS,LB2016MIS,LB2019DHS&lang=en&f=json")

ex2 <- data.frame(readdata)