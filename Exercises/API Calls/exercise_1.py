# -*- coding: utf-8 -*-
"""
Created on Fri Jan  6 11:49:35 2023

Python code to create and compile a data set for exercise 2 - gather data from a DHS API Call

@author: Grant
"""


import pandas as pd
import json, urllib.request, urllib
url_query=r'https://api.dhsprogram.com/rest/dhs/data?breakdown=national&indicatorIds=FE_FRTR_W_TFR&countryIds=EG,LB&surveyIds=EG2005DHS,EG2008DHS,EG2014DHS,LB2007DHS,LB2009MIS,LB2011MIS,LB2013DHS,LB2016MIS,LB2019DHS&lang=en&f=json'
t_data_json = urllib.request.urlopen(url_query)
t_data_read = json.loads(t_data_json.read())
t_data_item = t_data_read['Data']
ex2solved= pd.DataFrame(t_data_item)