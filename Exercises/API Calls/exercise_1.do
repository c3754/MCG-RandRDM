//Stata syntax to create and compile a dataset for exercise #2 using the DHS API//
clear
set more off
//Specify the variables you'll eventually import

gen str20 TFR15_49=""
gen str20 DHS_CountryCode=""
gen str20 Year=""

insheetjson TFR15_49 Year DHS_CountryCode using"https://api.dhsprogram.com/rest/dhs/data?breakdown=national&indicatorIds=FE_FRTR_W_TFR&countryIds=EG,LB&surveyIds=EG2005DHS,EG2008DHS,EG2014DHS,LB2007DHS,LB2009MIS,LB2011MIS,LB2013DHS,LB2016MIS,LB2019DHS&lang=en&f=json", table(Data) columns(Value SurveyYearLabel DHS_CountryCode)

replace Year="2019" if Year=="2019-20"
destring TFR15_49, replace
destring Year, replace
