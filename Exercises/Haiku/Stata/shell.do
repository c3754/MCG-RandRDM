//Shell to write a Haiku in Stata and export to a txt file. (Note that putdocx, or putpdf are also options)

cd "F:/CRDCN data/Training on research management and reproducibility/Exercises/Haiku/Stata/code"

dyndoc "Statahaiku.txt", saving (statahaiku.html) rep

*This ends our demonstration

*Note that the "putdocx" functionality of Stata can replicate this functionality, but that it is restricted to output in MSword format. There are latex plugins that work on estout and other stata features. This is the most basic way to conduct this operation, but there is lots of functionality to work on dynamic documents within stata.