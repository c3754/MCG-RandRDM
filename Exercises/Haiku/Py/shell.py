# -*- coding: utf-8 -*-
"""
Created on Fri Dec 16 12:10:43 2022

@author: Grant

Shell program to write a haiku using three sub-processes

This process is an exemplar of the exercise in the workflow and reproducibility workshop.

"""


"""
Load the required packages
"""

"Call line #1"

execfile('Code\line1.py')

"Call line #2"

execfile('Code\line2.py')

"Call line #3"

execfile('Code\line3.py')


"Print the doc"

execfile('Code\PrintPy.py')
