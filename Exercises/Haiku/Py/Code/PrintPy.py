"Add the results to an object, change the directory to where things will be printed and print them to a file"

results = [line1, line2, line3]

with open('haiku.txt', 'w') as text:
    text.write('\n'.join(results))
