# -*- coding: utf-8 -*-
"""
RSclean2.1

Description: Python code to import and clean data from the CRDCN researchers survey

Dependencies: Researcher survey data from the current year and the year before
    Export settings from LS: Export Labels, Question Codes. Export all survey questions, and tokens, do not export timestamps. 
    
In-state: Raw survey data
Out-state: Python environment with 2 data-frames. One for users and one for non-users (userdata & nonuserdata).

Process: Split sample into users & non-users, apply previous year response data for non-time-varying info, label & rename variables

Date created: 2022-03-14
Date last run: 2023-02-27

Oustanding bugs:
    - Software subquestions fail import 
    - ORCID data do not contain leading zeroes for API call (insert conditionally or fix import style numeric) 
"""
# Bonus easter egg! Because we were talking directories here, I want to show how things operate. I can use python to tell me what the current directory is:
    
import os
print (os.getcwd())

#If you downloaded the repo and didn't mess around with where things are, we should be in the head of this project\Exercises\Example
#What if we wanted to call our shell for the python Haiku?

execfile ('..\Haiku\Py\Shell.py')

"""
The two dots tell python to look in the parent folder to the current folder (i.e. we're in exercises\example, so the parent is just "exercises").
I want python to look in Haiku, so I first need to go to the parent then into the Haiku file.
Note, the execfile will just do what it's told, but since the python shell doesn't have any directory re-mapping, it's looking in "exercises\example" for a folder called "code" with contents "line1.py"
Best if the shell file is in the root of the directory, and everything else is specified with 'relative paths'.
Python has some options for manipulating the cd inside the "os" package. 

Note that this file doesn't run because I can't have both a simple looking shell.py and call it from within another directory, but it is possible to make shell.py work by mapping the directory to an item and calling using that returned directory!
"""
