---
title: "Workflow reproducibility and RDM or: How I learned to stop worrying and love my fellow scientists"
author: "Grant Gibson"
date: '`r paste("Date:",Sys.Date())`'
keep_tex: "FALSE"
header-includes:
output: 
 beamer_presentation:
  includes: 
    in_header: Content/CRDCNtheme.tex
    slide_level: 2
    fig_caption: FALSE
---

# Good morning!

## Warm up 1 - brainstorming
What do you think of when you hear the words "open science"?

What do you think of when you hear "reproducibility"? "Replicability"?

## Warm up 1 - brainstorming
- *Open tools*
- *Open data*
- *Open access*
- Transparency
- Science as a public good

Reproducibility: Same data, same methods, same exact answer

Replicability: Different data same methods, similar answer (?)

Other concepts: Generalizability, robustness

Note: Open Science is not a binary state, it's a continuum between what was commonplace 25 years ago and what is possible today.


## Life of a research project
1. Formulate a research question/hypothesis
2. Develop a methodology
3. Gather research data
4. Analyse data
5. Interpret results -> start over at 1(maybe?)

We're going to concern ourselves mostly with 3 and 4 today, but we need to briefly pay some lip service to what happens between 2 and 3.

## Introduction

1. Research Data Management
    
- Strategies
- DMPs

2. Reproducible Research
    
- Use of repositories
- Strategies for organization

What you will be equipped to go forward on your own with:
  
  1. Learning about supports at your institution for RDM
  2. Figuring out how to cite and share the data you use
  3. Develop your own workflow and process for research output
  4. Improving your skills with repositories and version control

## Goals
Starting from today each of you will (at a minimum):

1. Always cite the data you are using 
2. Understand the basics of RDM and know how best to share with your readers given your project
3. Set up your workflow to enhance reproducibility/productivity
4. Never have a current file called "final draft (final2_GG_RP).docx" ever again (at least not when working with sensibly compliant co-authors)

## Motivation

Brief appeal to altruistic motivations
  
  - Science as an institution
  - Helping others in your field understand your contributions
  
Moving on to selfish motivations
  
  - You'll need to learn this in order to publish well
  - You need to conduct projects in a way that let you figure them out (or at least let your RA's do so when you're a big shot)
  
## Don't let this...
![Headline: "I shot at my own foot with my own gun"](Content/retraction_watch_img.png)

## or this, be you!
![](Content/early_version_issue.png)
  
## Trends & Sensibility

Massive shakeup in the way tech and social science interact
  
  - Methodological sophistication
  - Workflow
  - Data sharing

Navigating these waters can be tough
  
  - Having principles is a HUGE help

## Warm up 2 - sourcing data
I am told that a commonly used data source is the Demographic & Health survey. Partner up and tell me:

  1. Where I can find it? (Is there more than one place?)
  2. How should I cite it if I were to use it in a paper?
  3. How might *you* go about making sure someone else can find the data you used (aside from via the citation)?
  
## Back together

Answer me these questions three:

  1. Where I can find it? (Is there more than one place)
  2. How I should cite it if I were to use it in a paper?
  3. How might *you* go about making sure someone else can find the data you used (aside from via the citation)?
  
---

## Where can I find it? 

- Aggregate statistics are dime a dozen.
  1. STATcompiler.com
  2. DHSprogram.com
  3. Mobile application
  4. *API*

- Restricted survey data (should be) only available through the DHS site.
  - If you find it elsewhere you should probably report it to DHS!
- Is one of these things worse than the others? How would you rank these?

## How I should cite it?

- [Use the Citation guide](https://dhsprogram.com/publications/Recommended-Citations.cfm)

>ex. ICF. 2004-2017. Demographic and Health Surveys (various) [Datasets]. Funded by USAID. Rockville, Maryland: ICF International.

## Data citations
A data citation should include, at the very least, the following elements:

- Author(s): the creator(s) of the dataset, in priority order. May be an institution or person(s).
If extant, the creator should include a "nameIdentifier," such as an Open Researcher and Contributor ID (ORCID) or International Standard Name Identifier (ISNI)
- Publication/Release date: Whichever is the later of: the date the dataset was made available, the date all quality assurance procedures were completed, and the date the embargo period (if applicable) expired.
- Title: the formal title of the data set

## Data citations cont'd
- Version: the precise version of the data used. Careful version tracking is critical to accurate citation.
- Publisher/Archive/Distributor: the organization distributing or hosting the data, ideally over the long term
- Identifier: a unique string that identifies the resource; should be a persistent scheme such as a DOI (10.1234/8675309), handle, or ARK (www.example.org/ark:/12345/lucky777).
- Access Date: because data can be dynamic and changeable in ways that are not always reflected in release dates and versions, it is important to indicate when on-line(sic) data were accessed.

[*Thanks to OSU for this list*](https://guides.library.oregonstate.edu/research-data-services/data-management-data-citation)

## Data citation cont'd
- But what about datasets that don't give you this info?
  *Ex. Statcan microdata*
- Do the best you can. These datasets will still often have a recommended citation.

Statistics Canada. 2001. *General Social Survey Cycle 15* (master file). Record Number 4501. https://www23.statcan.gc.ca/imdb/p2SV.pl?Function=getSurvey&Id=5272. Accessed at the McGill-Concordia RDC January 15, 2021.

- Note, this does not precisely match the recommended citation from Statistics Canada

## How *you* might go about making sure someone else can find the data you used (aside from via the citation)?

- Data access statements
- *API* call within the code
- *API* call outside the code
- Data deposit?

# Managing (or not) data

## Data deposit
- Many systems in place to do this (ex. Borealis & FRDR)
- University library support
- Some tips:
  - Don't re-deposit something that is archived
  - Make sure you are behaving within the law!

![Terms of use for the Demographic & Health Survey Data](Content/DHS_TOU.jpg)

## API calls
- Query-language based automated data requests
- Can provide data in a bunch of formats (ex. JSON (and derivatives), xml, HTML)
- How to use? Different for each dataset, but everybody provides [instructions](https://api.dhsprogram.com/#/introapi.cfm)
- DHS RESTful API calls, also accepts Query language (Representational State Transfer - just an architecture)
- Better than trying to simply describe what you did to get the data; repeatable and baked into your code!
- Some tweaks around the edges may be needed for some output

## API calls programming
- 1 or 2 step process (usually)
1. Tell software to go to a URL to find and import the (machine readable formatted) data.
2. Convert that not-so-usefully formatted data into something that you can analyze.

- Depending on the result of the API you may find it sensible to do more than one call or format multiple calls in a specific way (ex. behaviour might be different for a balanced panel than what we're about to do)
- If this is too much, give a link to JSON or XML versions of the data if possible in your DAS.

## Using JSON data within R, Stata & Python
- In R:
  
  `require (jsonlite)`
  
  `importdata <-fromJSON(*API URL HERE*)`
  
- Note, there are a plethora of packages to read JSON in both R and Python, figure out what aligns best with your way of thinking.  
  
## Using JSON data within R, Stata & Python
- In python3:

  `import json, urllib2, urllib, pandas`

  `url_query = r'*API URL goes here*'`

  `data_json = urllib2.urlopen(url_query)`

  `data_read = json.loads(data_json.read())`

  `data_item = data_read['Data']`
  
## Using JSON data within R, Stata & Python
- In stata:

    `*Pre-define variables*`

    `ssc install insheetjson`

    `ssc install libjson`

    `insheet json using "*API URL goes here*, weird option specifications required"`

    `}`

(helpfully, you can use the limit(20) option to make sure your call is good before you drop that option to import all the data. If you have Stata 17 I would switch over to python to do API work).

## API calls for your own data
- Lots of repositories support API calls to deposit/pull down data
- Borealis has [extensive documentation](https://borealisdata.ca/guides/en/latest/api/getting-started.html) and even its own [plugins](https://borealisdata.ca/guides/en/latest/api/client-libraries.html) for python and R

## Data access statement
- A doc to describe how to get the data *you started with*
- Follow the KISS principle within reason
- What to include:
  1. Where can someone go to get the data & what do they do to get what you got?
  2. What restrictions are there on who can access it and how?
  3. Why are these restrictions in place?
  4. Citation for the data
- Note, you should provide this doc even if you do the things we just talked about
  - Why?

## What if I'm providing the data?
- We'll discuss during the DMP section, but lots to consider.

## Qualitative data
- Disclaimer: I am not and have never been a qualitative data person. I have one qualitative project under my belt and it's 0% confidential/sensitive.
- Some guiding principles:
1. Not an excuse to forget about transparency in research
2. Be overly conservative in estimating the risks of sharing
3. DMP and thinking about sharing is critical in the early stages of the project (even moreso than usual)
4. Infrastructure exists to help you manage and in some cases share data securely (ZKE)

## Exercise API call
- Write an API call to the DHS system using the software of your choice to pull data and create a dataset from Egypt and Liberia on total fertility rates for ages 15-49 for years after 2004.

## Break
- Take five: stretch, chat and don't think about RDM for a bit

## API Call - take up

# DMPs
- Note for the McMaster workshop on 8-6-23 Isaac Pratt's slides were presented. We did not cover the DMP slide specifically.
## DMP (data management plan)
- What is it?
- [Where do I go?](https://assistant.portagenetwork.ca/)
- How do I do it? (sweet sweet resources at the link)
- Advice (what tips do you have old man?)
  - Follow the template
  - Think about resources (a lot are provided for you, but maybe your project needs more?)
  - Think back to our first exercise, in soc-sci you can do lots with a little
  - Think about how you will [store your data](https://library.duke.edu/using/policies/recommended-file-formats-digital-preservation)

# VC and repos
## Version control
- Distributed vs. centralized repositories
- Lock vs. merge models
- Learning git

## Let's git you set up
Open a terminal (I don't care where) open R-studio, open any Command line interface/terminal (except stata), let's go!

`git config --global user.name "*YOUR NAME HERE*"`

`git config --global user.email "*YOUR@email.here*"`

Did you bring your research project? Navigate to (a copy of) the files
  `cd "?:/where/are/your/files/"`

  `git init` creates a git repository in the current directory (need only be done once)

  `git add "filename in the root directory.extension"` (begins *staging* for a commit - can also use add "--all", ".", or "-A" as shorthand, combine with gitignore)

  `git commit -m "your committing message here"` (takes a snapshot of the files)

## That's the basics
- You can set up an online space to host your content
- Let's pull down the repo for this workshop with git clone
- `cd` yourself into a new space or use a p&c interface if you want.

![](Content/gitgui.jpg)

## Activity time
Let's do a few local repos on your system.
  
  1. Pull the repo for this course, add your name below this instruction and commit the change to your system to make it your own! (https://gitlab.com/c3754/ReproducibilityandRDM.git)
    - Anatomy: gitlab.com *git repository system*/ c3754 *That's me!* / ReproducibilityandRDM.git *That's the repo!*
    - If you ever want to see if I've made an update to this course you can just cd into the repository and type `git pull`
  
  2. Create a local repo with (a copy of) your project files.
  
## Let's git you going (activity - create a remote repository)
- Login to your git account
- Set up a new project
- Move your files into the new repo
- If time, set up a push (maybe on your own?) 

## Demo from my system
- Going to create an edit to a file on my laptop then commit and push it to gitlab
- Login to gitlab to confirm that the change is reflected there
- Undo the change to the file then commit
- git pull from my system and confirm the edit is deleted

## What else does this let you do?
- Share your code (as you just saw) with people you've never met
- Recover to backup points: 
  -`git checkout *Commit tag*`
  - online repository (i.e. backups: 3-formats in 2-locations! Do you?)
- Collaborate with co-authors (or yourself) without using any emails or thumbsticks.
  - SSH authentication using keys as you just saw (operating system dependent, need to store the key inside the online repository)
  
## Other implementation
- Baked into R-studio with R projects (which can include markdown, python, Rcode) with point & click functionality
- Insanely silly in Stata because you need to write and update some canned BASH program to call as Stata cannot interact with the terminal in an interactive way.
- GitPython (still need to set up your .ssh)
- GitGui
- Suggestion: Learn to use the terminal over time, but use gitgui to get into the flow

## Other useful git commands
`git reset (*commit name*)` restores to a previous state deleting everything afterwards

`git status` lists all the files that are staged

`git log` lists full history of commits

`git push` update the remote repository to the latest local commit

`git pull` update the local repository to the latest remote commit

`git grep "search term"` search the directory

## Final thoughts on .git
- Anecdote: old dogs & new tricks
- Gitlab/hub is not a repository; one last step when you sunset a project!
- Working on different workstations
  1. Start the morning with a `git pull`
  2. `git commit` as appropriate throughout the day
  3. `git commit` then `git push` at day's end

Personally, I use R-studio which gives me a series of terminals that I can rename (demo)

## A portrait of the presenter as a young man
- Stephen Dedalus did his entire thesis in a secure facility and mostly works in secure research environments to this day
  - Tools are variously available
  - Archiving/reproducibility is an afterthought
- Lessons learned in avoiding a quagmire

# Making a nice reproducible repo

## Start with a readme
- You'll get the hang of this, but check my sample readme and [this template](https://github.com/social-science-data-editors/template_README/blob/development/template-README.md)
- It's purpose is to help someone understand what is in your repository and how to use it.
- Minimum (in my view): Overview/Summary/Purpose, Contents & Use, Computing environment (including software). 

## What do you need
- Everything we've discussed so far!
  - Data if you can/should (deposited in the most accessible way possible)
  - Code: all of it that produced the output (try not to clutter, if feasible run it!)
  - Your readme 
  - Data access statement

## Making it reproducible
- Version control your packages R:([packrat](https://rstudio.github.io/packrat/) and [checkpoint](https://cran.r-project.org/web/packages/checkpoint/vignettes/checkpoint.html)), Python:[pip-tools](https://pip.pypa.io/en/stable/topics/repeatable-installs/), Stata:pack up ado files, version your do file ("version 17")
- Wrangle the randomness
- Make it idiot-proof 
    
    R: `source(analysis.R)` 

    Python: `execfile('analysis.py')` 
    
    Stata: `do "analysis.do"`

## Making it reproducible cont'd
- Automate everything you can, run your shell file to get your results.
- Dynamic document generation with in-document code and figures/ analysis. Worth the effort to learn Jupyter or markdown. RMD (where these slides were created) is just smoother at this point in time for academic work. Dynamic doc generation released in Stata 17.

## Shell file exercise
- Make a folder and, using one shell file to call several programs, write a haiku using a different file to separately write each line with a single click.
  - remember it's 5-7-5 syllables ;)

- If you have extra time, number your poem using a random number generator and make sure it's numbered the same every time!

## Examples with each software?
- You'll find some examples in the repo, How did you structure things?

# Structure
## Current state of play
- How are your projects set up now?

## While in process
- One file to clean data (maybe a few if you're merging data)
- One file per table/figure name them what they do (ex. "Table1.R")
- A quick chat about filepaths

## Headers?
- Give yourself a "date last run" somewhere (maybe you make this a text file using the shell?)
- Purpose? Input/output descriptions? In state & outstate?

## Tips?
- Define things as items or set local macros as much as possible
- Things that might be modified should be defined as items at the top
  - Ex. Maybe you're simulating a model with 2/3/4% growth? Specify your model with parameter placeholders and loop over it.
- Comments must be informative, must be kept up to date! A piece of code with a useful description at the top beats one with comments throughout but no indication of what it's for.

## What's the point?
- Helps you when things go wrong (or when they go according to plan - R&Rs or major errors)
- Helps others navigate your project (including people you might hire)
- Major reduction in probability to make a mistake (especially if you automate your table output!)

## But figure out what works for you
- Whatever you settle into make sure it solves those problems
- Organization and structure is up to you, but think about how to manage directories in a way that won't make you pull your hair out.
- It may not be the same thing each time. My projects using confidential data are vastly different from projects using open data/data I've collected
- Sermon of the Dean of St. Paul "No man is an island"

## Organization time!
- Take your project code (the copy in the repo) and do a re-org! Write a quick readme and organize/comment things appropriately. Open dismissal, I'm here for questions.

- Also happy to help anyone set up a ssh key and a push

## Reminders
- Shell files
- Dates?
- State in state out?
- Number of files?
- Readme?
- Comments?
- Headers?

## Thank you
- Thank you for your attention
- Questions?

## Misc. Credits
- I owe much of my knowledge to pioneers and envelope pushers in this area whom I have read and/or seen present. The remainder is cobbled together from mistakes made and independent inquiry.
- The code principles page in the resources for this workshop were created by Jeremy Freese at Stanford. I tweaked his HTML a bit for local access, and expanded his tips.