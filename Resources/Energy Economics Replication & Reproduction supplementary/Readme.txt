Feb 10 2017

This zip file contains a Markdown example. If you install TeX, R, and RStudio on your system, you can load the R Project file

energy.Rproj

in RStudio, from the files tab (lower right pane) open the Rmd script energy.Rmd, then click the knit icon.

If things don't render as you expect, try the following:

1)      Install the latest version of pandoc on your system
 
https://github.com/jgm/pandoc/releases/tag/1.19.2.1
 
2)      Install the latest development version of RStudio on your system
 
Google "rstudio daily builds"
 
3)      Install the latest development versions of a few packages – in R/RStudio run the following in the console
 
devtools::install_github('yihui/knitr')
devtools::install_github('rstudio/bookdown')
devtools::install_github('rstudio/markdown')
devtools::install_github("rstudio/rticles")

If that still does not work, feel free to email me at racinej@mcmaster.ca
